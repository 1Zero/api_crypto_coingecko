import 'package:api_crypto_coingecko/config/palette.dart';
import 'package:api_crypto_coingecko/features/api_coingecko_crypto/presentation/widgets/beta.dart';
import 'package:flutter/material.dart';


 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Crypto App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Palette.scaffold
      ),
      home: ApiCryotp(),
    );
  }
}
