import 'package:api_crypto_coingecko/features/api_coingecko_crypto/data/models/crypto_balance_json.dart';
import 'package:api_crypto_coingecko/features/api_coingecko_crypto/data/repositories/api_coingecko.dart';
import 'package:flutter/material.dart';

//import 'features/api_coingecko_crypto/data/repositories/api_coingecko.dart';

void main() => runApp(ApiCryotp());

class ApiCryotp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<ApiCryotp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('api cryptos'),
        ),
        // body: Container(
        //   child: FutureBuilder<Covalenthq>(
        //     future: getUserData(),
        //     builder: (context, snapshot) {
        //       if (snapshot.connectionState == ConnectionState.done)
        //           Text('${snapshot.data!.data.address}');

        //         return CircularProgressIndicator();
        //     },
        //   ),
        // ),
        body: Container(
          child: Column(
            children: [
              //Id paciente
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(12.0),
                    child: FutureBuilder<Covalenthq>(
                      future: getUserData(),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done)
                          return Text(
                              'id:  ${snapshot.requireData.data.address} ' );
                        else
                          return CircularProgressIndicator();
                      },
                    ),
                  ),
                ],
              ),

// NoSuchMethodError (NoSuchMethodError: The method 'toDouble' was called on null.
// Receiver: null
// Tried calling: toDouble())

             // Nombre
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(12.0),
                    child: FutureBuilder<Covalenthq>(
                      future: getUserData(),
                      builder: (context, snapshot){
                        if(snapshot.connectionState ==ConnectionState.done)
                        return Text('intercambio: ${snapshot.requireData.data.items[0].contractName} ');
                        else
                        return CircularProgressIndicator();
                      }
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
