import 'package:api_crypto_coingecko/config/palette.dart';
import 'package:api_crypto_coingecko/features/api_coingecko_crypto/presentation/widgets/beta.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            title: Text(
              'Transacciones',
              style: const TextStyle(
                color: Palette.facebookBlue,
                fontSize: 28.0,
                fontWeight: FontWeight.bold
              ),
              
            ),
            
          ),
          
        ],
        
      ),      
    );
  }
}