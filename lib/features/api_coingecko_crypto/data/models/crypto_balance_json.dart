// To parserequired this JSON data, do
//
//     final covalenthq = covalenthqFromJson(jsonString);

import 'dart:convert';

Covalenthq covalenthqFromJson(String str) =>
    Covalenthq.fromJson(json.decode(str));

String covalenthqToJson(Covalenthq data) => json.encode(data.toJson());

class Covalenthq {
  Covalenthq({
   required this.data,
   required this.error,
   required this.errorMessage,
   required this.errorCode,
  });

  Data data;
  bool error;
  dynamic errorMessage;
  dynamic errorCode;

  factory Covalenthq.fromJson(Map<String, dynamic> json) => Covalenthq(
        data: Data.fromJson(json["data"]),
        error: json["error"],
        errorMessage: json["error_message"],
        errorCode: json["error_code"],
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "error": error,
        "error_message": errorMessage,
        "error_code": errorCode,
      };
}

class Data {
  Data({
   required this.address,
   required this.updatedAt,
   required this.nextUpdateAt,
   required this.quoteCurrency,
   required this.chainId,
   required this.items,
   required this.pagination,
  });

  String address;
  DateTime updatedAt;
  DateTime nextUpdateAt;
  String quoteCurrency;
  int chainId;
  List<Item> items;
  dynamic pagination;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        address: json["address"],
        updatedAt: DateTime.parse(json["updated_at"]),
        nextUpdateAt: DateTime.parse(json["next_update_at"]),
        quoteCurrency: json["quote_currency"],
        chainId: json["chain_id"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        pagination: json["pagination"],
      );

  Map<String, dynamic> toJson() => {
        "address": address,
        "updated_at": updatedAt.toIso8601String(),
        "next_update_at": nextUpdateAt.toIso8601String(),
        "quote_currency": quoteCurrency,
        "chain_id": chainId,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "pagination": pagination,
      };
}

class Item {
  Item({
   required this.contractDecimals,
   required this.contractName,
   required this.contractTickerSymbol,
   required this.contractAddress,
   required this.supportsErc,
   required this.logoUrl,
   required this.type,
   required this.balance,
   required this.balance24H,
   required this.quoteRate,
   required this.quoteRate24H,
   required this.quote,
   required this.quote24H,
   required this.nftData,
  });

  int contractDecimals;
  String contractName;
  String contractTickerSymbol;
  String contractAddress;
  dynamic supportsErc;
  String logoUrl;
  String type;
  String balance;
  dynamic balance24H;
  double quoteRate;
  double quoteRate24H;
  double quote;
  dynamic quote24H;
  dynamic nftData;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        contractDecimals: json["contract_decimals"],
        contractName: json["contract_name"],
        contractTickerSymbol: json["contract_ticker_symbol"],
        contractAddress: json["contract_address"],
        supportsErc: json["supports_erc"] == null
            ? null
            : List<String>.from(json["supports_erc"].map((x) => x)),
        logoUrl: json["logo_url"],
        type: json["type"],
        balance: json["balance"],
        balance24H: json["balance_24h"],
        quoteRate: json["quote_rate"],
        quoteRate24H: json["quote_rate_24h"],
        quote: json["quote"],
        quote24H: json["quote_24h"],
        nftData: json["nft_data"],
      );

  Map<String, dynamic> toJson() => {
        "contract_decimals": contractDecimals,
        "contract_name": contractName,
        "contract_ticker_symbol": contractTickerSymbol,
        "contract_address": contractAddress,
        "supports_erc": supportsErc == null
            ? null
            : List<dynamic>.from(supportsErc.map((x) => x)),
        "logo_url": logoUrl,
        "type": type,
        "balance": balance,
        "balance_24h": balance24H,
        "quote_rate": quoteRate,
        "quote_rate_24h": quoteRate24H,
        "quote": quote,
        "quote_24h": quote24H,
        "nft_data": nftData,
      };
}
